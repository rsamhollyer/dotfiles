#  _______ _______ _______ _______ _______
# |\     /|\     /|\     /|\     /|\     /|
# | +---+ | +---+ | +---+ | +---+ | +---+ |
# | |   | | |   | | |   | | |   | | |   | |
# | |P  | | |I  | | |C  | | |O  | | |M  | |
# | +---+ | +---+ | +---+ | +---+ | +---+ |
# |/_____\|/_____\|/_____\|/_____\|/_____\|

# GLX backend

backend = "glx"
glx-no-stencil = true
glx-no-rebind-pixmap = false
use-damage = true

# Shadow

shadow = true;
shadow-radius = 20;
shadow-opacity = 0.55;
shadow-offset-x = -20;
shadow-offset-y = -20;
shadow-exclude = []

# Fading

fading = true
fade-in-step = 0.028;
fade-out-step = 0.06;
fade-delta = 10
fade-exclude = [
    "class_g = 'Polybar'"
]

# Opacity

inactive-opacity = 1.0
active-opacity = 1.0
opacity-rule =[
    "98:class_g = 'Code - Insiders'",
    "90:class_g = 'Terminator'",
    "90:class_g = 'Tilix'",
    "90:class_g = 'Lxterminal'",
    "90:class_g = 'Sakura'",
    "90:class_g = 'Xfce4-terminal'",
    "90:class_g = 'NVIM'",
    "90:class_g = 'Spacefm'",
    "95:class_g = 'Thunar'",
    "90:class_g = 'Rofi'"
]

# Background blurring
background-frame = true;
background-fixed = true;
blur: {
	method = "dual_kawase";
    strength = 4.0;
    background = true;
    background-frame = true;
    background-fixed = false;

}

blur-background-exclude = [
"window_type = 'dock'",
"window_type = 'desktop'"

]

# Other

vsync = true
mark-wmwin-focused = true
mark-ovredir-focused = true
detect-rounded-corners = true
detect-client-opacity = true
use-ewmh-active-win = false
unredir-if-possible = false
detect-transient = true
detect-client-leader = true
invert-color-include = []
corner-radius = 15



# Window type settings

wintypes:
{
    tooltip = { fade = true shadow = true opacity = 0.75 focus = true full-shadow = false }
    dock = { fade = true shadow = false }
    dnd = { fade = true shadow = false }
    popup_menu = { fade = true opacity = 1.0 }
    dropdown_menu = { fade = true opacity = 1.0 }
    utility       = { fade = true }
    dialog        = { fade = true }
    notify        = { fade = true }
    unknown       = { fade = true }
    notification  = { full-shadow = true }
}
